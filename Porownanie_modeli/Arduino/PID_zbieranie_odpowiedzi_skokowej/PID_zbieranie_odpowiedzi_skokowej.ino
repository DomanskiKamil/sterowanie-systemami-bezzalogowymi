long timebu[200];
long input[200];
long output[200];
bool firstHalfFull = false;
bool secondHalfFull = false;
int i = 0;
bool endstate = false;

portMUX_TYPE Mutex = portMUX_INITIALIZER_UNLOCKED;

uint8_t old_AB_state = 0;
static int8_t enc_states[] = { 0,-1,1,0,1,0,0,-1,-1,0,0,1,0,1,-1,0 };
int encoderCounts = 0;

float setPoint_RPM = 0;
float change = 200;


#define ROTARYENCODER_A_PIN 32
#define ROTARYENCODER_B_PIN 35

#define MOTOR_DIR_PIN 26
#define MOTOR_PWM_PIN 27
#define MOTOR_PWM_CHANNEL 0
#define FULL_REVOLUTION_COUNTS 1216

hw_timer_t * timer = NULL;

void IRAM_ATTR encoder_ISR(){
  portENTER_CRITICAL_ISR(&Mutex);
  old_AB_state <<= 2;
  int8_t ENC_PORT = ((digitalRead(ROTARYENCODER_A_PIN)) ? (1 << 1) : 0) | ((digitalRead(ROTARYENCODER_B_PIN)) ? (1 << 0) : 0);
  old_AB_state |= ( ENC_PORT & 0x03 );
  encoderCounts += ( enc_states[( old_AB_state & 0x0f )]);  
  portEXIT_CRITICAL_ISR(&Mutex);
}


float P = 6;
float I = 50;
float D = 0;
float integral = 0;
int lastError = 0;
float periodPID = 0.01; //in seconds

float lastoutput = 0;
#define MAX_OUTPUT 1023
#define MAX_INTEGRAL MAX_OUTPUT

bool go = false;


uint32_t cp0_regs[18];


void IRAM_ATTR control(){
  portENTER_CRITICAL_ISR(&Mutex);
  uint32_t cp_state = xthal_get_cpenable();
  
  if(cp_state) {
    // Save FPU registers
    xthal_save_cp0(cp0_regs);
  } else {
    // enable FPU
    xthal_set_cpenable(1);
  }
  
  int currentMotorSpeed = encoderCounts;
  output[i]= encoderCounts;
  encoderCounts = 0;
  int setPoint_EncoderCounts = (int)(setPoint_RPM*(FULL_REVOLUTION_COUNTS/(60.0*100))); 
  timebu[i] = millis();
  input[i] = setPoint_EncoderCounts;
  
  i++;
  if(i==100){
    firstHalfFull = true;
  }
  if(i>=200){
    secondHalfFull = true;
    i=0;
  }
 
  int error = setPoint_EncoderCounts - currentMotorSpeed;

  float curentIntegralPart = error * periodPID;
  integral += curentIntegralPart;

  float derivative = (error - lastError)/periodPID;
  float output = P*error + I*integral + D*derivative;

  if(output > MAX_OUTPUT) {
    integral -= curentIntegralPart;
    output = MAX_OUTPUT;
  }
  if(output < -MAX_OUTPUT){
    integral -= curentIntegralPart;
    output = -MAX_OUTPUT;
   
  }
  //Wystawiamy obliczoną wartość PWM z uwzględnieniem kierunku obrotu
  if(output < 0){
    digitalWrite(MOTOR_DIR_PIN, HIGH);
    ledcWrite(MOTOR_PWM_CHANNEL, (int)(-1*output));
  }
  if(output >= 0){
    digitalWrite(MOTOR_DIR_PIN, LOW);
    ledcWrite(MOTOR_PWM_CHANNEL, (int)(output));
  }
  lastoutput = output;
  lastError = error;

  if(cp_state) {
    // Restore FPU registers
    xthal_restore_cp0(cp0_regs);
  } else {
    // turn it back off
    xthal_set_cpenable(0);
  }
  portEXIT_CRITICAL_ISR(&Mutex);
}


void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  
  pinMode(ROTARYENCODER_A_PIN, INPUT);
  attachInterrupt(digitalPinToInterrupt(ROTARYENCODER_A_PIN), encoder_ISR, CHANGE);
  pinMode(ROTARYENCODER_B_PIN, INPUT);
  attachInterrupt(digitalPinToInterrupt(ROTARYENCODER_B_PIN), encoder_ISR, CHANGE);

  pinMode(MOTOR_DIR_PIN, OUTPUT);
  digitalWrite(MOTOR_DIR_PIN, LOW);

  ledcSetup(MOTOR_PWM_CHANNEL, 20000, 10);
  ledcAttachPin(MOTOR_PWM_PIN, MOTOR_PWM_CHANNEL);

  timer = timerBegin(0, 80, true);
  timerAttachInterrupt(timer, &control, true);
  timerAlarmWrite(timer, 10000, true);
  timerAlarmEnable(timer);


}
int lastTime = millis();
int lastTime2 = millis();

void loop() {
  int timeNow = millis();
  if((timeNow-lastTime >= 500) && (endstate == false)){
    setPoint_RPM = 350;
    if(timeNow - lastTime >= 3000){
      setPoint_RPM = 0;
      endstate = true;
    }
  }
  if((firstHalfFull==true)&& (endstate == false)){
    firstHalfFull=false;
    for(int j=0;j<100;j++){
      Serial.printf("%d,%d,%d\n",timebu[j],input[j],output[j]);
    }
  }
  if((secondHalfFull==true)&& (endstate == false)){
    secondHalfFull=false;
    for(int j=100;j<200;j++){
      Serial.printf("%d,%d,%d\n",timebu[j],input[j],output[j]);
    }
  }
}
