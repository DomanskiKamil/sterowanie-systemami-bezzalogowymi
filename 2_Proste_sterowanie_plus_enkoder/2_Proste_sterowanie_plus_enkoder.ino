#define MOTOR_DIR_PIN 26
#define MOTOR_PWM_PIN 27
#define MOTOR_PWM_CHANNEL 0

#define MAX_OUTPUT 1023
#define MAX_INTEGRAL MAX_OUTPUT

int dutyCycle = 0;

//-----------------------------
#define ROTARYENCODER_A_PIN 32
#define ROTARYENCODER_B_PIN 35

#define FULL_REVOLUTION_COUNTS 1216.0

uint8_t old_AB_state = 0;
static int8_t enc_states[] = { 0,-1,1,0,1,0,0,-1,-1,0,0,1,0,1,-1,0 };
int encoderCounts = 0;

portMUX_TYPE Mutex = portMUX_INITIALIZER_UNLOCKED;

//Funkcja obsługująca przerwanie
void IRAM_ATTR encoder_ISR(){
  //Wyłączenie możliwości występowania przerwań na czas wykonywania funkcji
  portENTER_CRITICAL_ISR(&Mutex);
  //Bitowe przesunięcie starego stanu o dwa bity w lewo 
  old_AB_state <<= 2;
  //Odczytanie stanu Pinu A i B oraz stworzenie z nich dwucyfrowej liczby binarnej, gdzie pierwsza cyfra to stan pinu A, a druga stan pinu B 
  int8_t ENC_PORT = ((digitalRead(ROTARYENCODER_A_PIN)) ? (1 << 1) : 0) | ((digitalRead(ROTARYENCODER_B_PIN)) ? (1 << 0) : 0);
  //Połączenie starego stanu i nowego. W konsekwencji powstaje czterobitowa liczba gdzie pierwsze dwie cyfry to poprzedni stan, a dwie kolejne to stan aktualny.
  old_AB_state |= ( ENC_PORT & 0x03 );
  //Na podstawie kombinacji poprzedniego i aktualnego stanu odczytujemy z przygotowanej tablicy wartość którą dodajemy lub odejmujemy od sumy wszystkich impulsów
  //W encoderCounts przechowujemy wszystkie zliczenia.
  encoderCounts += ( enc_states[( old_AB_state & 0x0f )]);  
  //Włączenie możliwości występowania przerwań
  portEXIT_CRITICAL_ISR(&Mutex);
}

//------------------------------




void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

  //-----------------------------
  //Ustawienie pinów połączonych z enkoderem jako wejścia
  pinMode(ROTARYENCODER_A_PIN, INPUT);
  
  //Podłączenie przerwania do pinu  ROTARYENCODER_A_PIN 
  //attachInterrupt("Pin na którego zmiane reagować ma przerwanie, "funkcja która ma zostać wywołana przy uruchomieniu przerwania", "zbocze/stan który ma wywołać przerwanie"
  //W tym przypadku przy każdym wywołaniu przerwania zostanie uruchomiona funkcja encoder_ISR, 
  //natomiast przerwanie wywoła każda zmiana stanu na pinie ROTARYENCODER_A_PIN (zbocze narastające jak i opadające)
  attachInterrupt(digitalPinToInterrupt(ROTARYENCODER_A_PIN), encoder_ISR, CHANGE);
  
  pinMode(ROTARYENCODER_B_PIN, INPUT);
  attachInterrupt(digitalPinToInterrupt(ROTARYENCODER_B_PIN), encoder_ISR, CHANGE);
  
  //------------------------------

  
  // Ustawienie Pinu DIR jako wyjście
  pinMode(MOTOR_DIR_PIN, OUTPUT);
  // Ustawienie na Pinie DIR stanu niskiego
  digitalWrite(MOTOR_DIR_PIN, LOW);

  //Konfiguracja wyjścia PWM
  //Ustawinie kanału PWM (Nr kanału, częstotliwość, 10 bitowa rozdzielczość)
  //10 bitowa rozdzielczość daje nam możliwość ustawienia PWM w zakresie 0 - 1023
  ledcSetup(MOTOR_PWM_CHANNEL, 20000, 10);
  //Podłącznie kanału PWM do PINU "MOTOR_PWM_PIN"
  ledcAttachPin(MOTOR_PWM_PIN, MOTOR_PWM_CHANNEL);
  //Ustaienie na wyjściu wypełnia o wartości 0 
  dutyCycle = 0;
  ledcWrite(MOTOR_PWM_CHANNEL, dutyCycle);

}

//------------------------------
int lastTime = millis();
int period = 50; //w [ms]
//------------------------------
void loop() {
  //Sprawdź czy przyszła jakaś wiadomość
  if (Serial.available() > 0) {
    //Ustawianie prędkości odbywa się przy użyciu komendy "p100"
    //Po znaku p występuje porządana wartość wypełnienia PWM
    char command = Serial.read();
    //Sprawdź czy wiadomość rozpoczyna się od znaku "p"
    if(command == 'p'){
      //Odczytaj liczbę odebraną przez UART
      int val = Serial.parseInt();
      //Sprawdź czy wartość mieści się pomiędzy dopuszczalnym minimum i maksimum
      if((val >= -MAX_OUTPUT) && (val <= MAX_OUTPUT)){
        dutyCycle = val;
        val = (int)(5*val/3);
        dutyCycle = val;
        if(val > 0){
          digitalWrite(MOTOR_DIR_PIN, LOW);
          ledcWrite(MOTOR_PWM_CHANNEL, val);
        }
        else{
          //Jeśli uzyskana warość jest ujemna to ustaw pin kirunku w stan wysoki
          digitalWrite(MOTOR_DIR_PIN, HIGH);
          //Oblicz wartość bezwzględną wartości, ponieważ wypenienie PWM musi być liczbą dodatnią
          int absoluteValue = abs(val);
          //Ustaw wartość na wyjście PWM
          ledcWrite(MOTOR_PWM_CHANNEL, absoluteValue);
        }
        //Wypisz w konsoli ustawioną wartość
        Serial.print("Ustawiono: ");
        Serial.println(val);
      }
      else{
        //Jeśli odebrana wartość nie mieści się w zakresie to wypisz błąd
        Serial.println("Podano złą wartość");
      }   
    }
  }
  //------------------------------
  int timeNow = millis();
  
  if(timeNow - lastTime >= period){
    //Ustawienia nowego miejsca w czasie od którego liczymy kolejny okres "period"
    lastTime = timeNow;
    //Obliczenie prędkości obrotowej na podstawie zliczeń enkodera
    float rpm = encoderCounts/FULL_REVOLUTION_COUNTS *(1/(period*0.001))*60;      
    //Wypisanie wartości wypełnienia sygnału PWM i zliczeń enkodera
    Serial.printf("%d,%f\n",dutyCycle,rpm);
    //Wyzerowanie licznika, aby co każdy "period" otrzymywać wartość impulsów zliczonych w tym czasie
    encoderCounts = 0;
  }

  //------------------------------


  
}
