long sampleTime[200];
long input[200];
long output[200];

bool firstHalfFull = false;
bool secondHalfFull = false;
int i = 0;

portMUX_TYPE Mutex = portMUX_INITIALIZER_UNLOCKED;

uint8_t old_AB_state = 0;
static int8_t enc_states[] = { 0,-1,1,0,1,0,0,-1,-1,0,0,1,0,1,-1,0 };
int encoderCounts = 0;

int setPoint = 0;


#define ROTARYENCODER_A_PIN 32
#define ROTARYENCODER_B_PIN 35

#define MOTOR_DIR_PIN 26
#define MOTOR_PWM_PIN 27
#define MOTOR_PWM_CHANNEL 0
#define FULL_REVOLUTION_COUNTS 1216

hw_timer_t * timer = NULL;

void IRAM_ATTR encoder_ISR(){
  portENTER_CRITICAL_ISR(&Mutex);
  old_AB_state <<= 2;
  int8_t ENC_PORT = ((digitalRead(ROTARYENCODER_A_PIN)) ? (1 << 1) : 0) | ((digitalRead(ROTARYENCODER_B_PIN)) ? (1 << 0) : 0);
  old_AB_state |= ( ENC_PORT & 0x03 );
  encoderCounts += ( enc_states[( old_AB_state & 0x0f )]);  
  portEXIT_CRITICAL_ISR(&Mutex);
}

uint32_t cp0_regs[18];

void IRAM_ATTR control(){
  portENTER_CRITICAL_ISR(&Mutex);
  uint32_t cp_state = xthal_get_cpenable();
  
  if(cp_state) {
    // Save FPU registers
    xthal_save_cp0(cp0_regs);
  } else {
    // enable FPU
    xthal_set_cpenable(1);
  }
  
  sampleTime[i] = millis();
  input[i] = setPoint;
  output[i]= encoderCounts;
  encoderCounts = 0;
  i++;
  if(i==100){
    firstHalfFull = true;
  }
  if(i>=200){
    secondHalfFull = true;
    i=0;
  }
  
  
 
  if(cp_state) {
    // Restore FPU registers
    xthal_restore_cp0(cp0_regs);
  } else {
    // turn it back off
    xthal_set_cpenable(0);
  }
  portEXIT_CRITICAL_ISR(&Mutex);
}


void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  
  pinMode(ROTARYENCODER_A_PIN, INPUT);
  attachInterrupt(digitalPinToInterrupt(ROTARYENCODER_A_PIN), encoder_ISR, CHANGE);
  pinMode(ROTARYENCODER_B_PIN, INPUT);
  attachInterrupt(digitalPinToInterrupt(ROTARYENCODER_B_PIN), encoder_ISR, CHANGE);

  pinMode(MOTOR_DIR_PIN, OUTPUT);
  digitalWrite(MOTOR_DIR_PIN, LOW);

  ledcSetup(MOTOR_PWM_CHANNEL, 20000, 10);
  ledcAttachPin(MOTOR_PWM_PIN, MOTOR_PWM_CHANNEL);

  timer = timerBegin(0, 80, true);
  timerAttachInterrupt(timer, &control, true);
  timerAlarmWrite(timer, 10000, true);
  timerAlarmEnable(timer);

}
int lastTime = millis();
int lastTime2 = millis();
int dutyCycle = 0;
int factor = 1;
bool endstate = false;
void loop() {
  
  int timeNow = millis();
  if((timeNow-lastTime >= 500) && (endstate == false)){
    setPoint = 604;
    ledcWrite(MOTOR_PWM_CHANNEL, setPoint);
    if(timeNow - lastTime >= 1500){
      setPoint = 0;
      ledcWrite(MOTOR_PWM_CHANNEL, setPoint);
      endstate = true;
    }
  }
  if((firstHalfFull==true)&& (endstate == false)){
    firstHalfFull=false;
    for(int j=0;j<100;j++){
      Serial.printf("%d,%d,%d\n", sampleTime[j],input[j],output[j]);
    }
  }
  if((secondHalfFull==true)&& (endstate == false)){
    secondHalfFull=false;
    for(int j=100;j<200;j++){
      Serial.printf("%d,%d,%d\n", sampleTime[j],input[j],output[j]);
    }
  }

}
