#define MOTOR_DIR_PIN 26
#define MOTOR_PWM_PIN 27
#define MOTOR_PWM_CHANNEL 0

#define MAX_OUTPUT 1023

int dutyCycle = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  // Ustawienie Pinu DIR jako wyjście
  pinMode(MOTOR_DIR_PIN, OUTPUT);
  // Ustawienie na Pinie DIR stanu niskiego
  digitalWrite(MOTOR_DIR_PIN, LOW);

  //Konfiguracja wyjścia PWM
  //Ustawinie kanału PWM (Nr kanału, częstotliwość, 10 bitowa rozdzielczość)
  //10 bitowa rozdzielczość daje nam możliwość ustawienia PWM w zakresie 0 - 1023
  ledcSetup(MOTOR_PWM_CHANNEL, 20000, 10);
  //Podłącznie kanału PWM do PINU "MOTOR_PWM_PIN"
  ledcAttachPin(MOTOR_PWM_PIN, MOTOR_PWM_CHANNEL);
  //Ustaienie na wyjściu wypełnia o wartości 0 
  dutyCycle = 0;
  ledcWrite(MOTOR_PWM_CHANNEL, 0);

}

void loop() {
  //Sprawdź czy przyszła jakaś wiadomość
  if (Serial.available() > 0) {
    //Ustawianie prędkości odbywa się przy użyciu komendy "p100"
    //Po znaku p występuje porządana wartość wypełnienia PWM
    char command = Serial.read();
    //Sprawdź czy wiadomość rozpoczyna się od znaku "p"
    if(command == 'p'){
      //Odczytaj liczbę odebraną przez UART
      int val = Serial.parseInt();
      //Sprawdź czy wartość mieści się pomiędzy dopuszczalnym minimum i maksimum
      if((val >= -MAX_OUTPUT) && (val <= MAX_OUTPUT)){
        dutyCycle = val;
        if(val > 0){
          digitalWrite(MOTOR_DIR_PIN, LOW);
          ledcWrite(MOTOR_PWM_CHANNEL, val);
        }
        else{
          //Jeśli uzyskana warość jest ujemna to ustaw pin kirunku w stan wysoki
          digitalWrite(MOTOR_DIR_PIN, HIGH);
          //Oblicz wartość bezwzględną wartości, ponieważ wypenienie PWM musi być liczbą dodatnią
          int absoluteValue = abs(val);
          //Ustaw wartość na wyjście PWM
          ledcWrite(MOTOR_PWM_CHANNEL, absoluteValue);
        }
        //Wypisz w konsoli ustawioną wartość
        Serial.print("Ustawiono: ");
        Serial.println(dutyCycle);
      }
      else{
        //Jeśli odebrana wartość nie mieści się w zakresie to wypisz błąd
        Serial.println("Podano złą wartość");
      }   
    }
  }
}
