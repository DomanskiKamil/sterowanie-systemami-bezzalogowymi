#define MOTOR_DIR_PIN 26
#define MOTOR_PWM_PIN 27
#define MOTOR_PWM_CHANNEL 0

#define MAX_OUTPUT 1023

#define ROTARYENCODER_A_PIN 32
#define ROTARYENCODER_B_PIN 35

#define FULL_REVOLUTION_COUNTS 1216.0

uint8_t old_AB_state = 0;
static int8_t enc_states[] = { 0,-1,1,0,1,0,0,-1,-1,0,0,1,0,1,-1,0 };
int encoderCounts = 0;

portMUX_TYPE Mutex = portMUX_INITIALIZER_UNLOCKED;

//Funkcja obsługująca przerwanie
void IRAM_ATTR encoder_ISR(){
  //Wyłączenie możliwości występowania przerwań na czas wykonywania funkcji
  portENTER_CRITICAL_ISR(&Mutex);
  //Bitowe przesunięcie starego stanu o dwa bity w lewo 
  old_AB_state <<= 2;
  //Odczytanie stanu Pinu A i B oraz stworzenie z nich dwucyfrowej liczby binarnej, gdzie pierwsza cyfra to stan pinu A, a druga stan pinu B 
  int8_t ENC_PORT = ((digitalRead(ROTARYENCODER_A_PIN)) ? (1 << 1) : 0) | ((digitalRead(ROTARYENCODER_B_PIN)) ? (1 << 0) : 0);
  //Połączenie starego stanu i nowego. W konsekwencji powstaje czterobitowa liczba gdzie pierwsze dwie cyfry to poprzedni stan, a dwie kolejne to stan aktualny.
  old_AB_state |= ( ENC_PORT & 0x03 );
  //Na podstawie kombinacji poprzedniego i aktualnego stanu odczytujemy z przygotowanej tablicy wartość którą dodajemy lub odejmujemy od sumy wszystkich impulsów
  //W encoderCounts przechowujemy wszystkie zliczenia.
  encoderCounts += ( enc_states[( old_AB_state & 0x0f )]);  
  //Włączenie możliwości występowania przerwań
  portEXIT_CRITICAL_ISR(&Mutex);
}

//----------------------------
hw_timer_t * timer = NULL;
uint32_t cp0_regs[18];

//Współczynniki sterownika 
float P = 15;
float I = 250;
float D = 0;
float periodPID = 0.01;

float integral = 0;
int lastError = 0;

int setPoint_RPM = -60;
int change = -300;

#define MAX_OUTPUT 1023
#define MAX_VELOCITY 500

int currentEncoderCounts = 0; 
int setPoint_EncoderCounts = 0;
void IRAM_ATTR control(){
  portENTER_CRITICAL_ISR(&Mutex);

  //Kawałek kodu związany tylko z esp32
  //Podczas przerwania występuje problem z koprocesorem do obilczeń zmiennoprzecinkowych
  //Należy skopiować jego stań przed przerwaniem i przywrócić go po realizacji przerwania
  uint32_t cp_state = xthal_get_cpenable();
  if(cp_state) {
    // Save FPU registers
    xthal_save_cp0(cp0_regs);
  } else {
    // enable FPU
    xthal_set_cpenable(1);
  }

  //Odczytanie stanu enkodera i zresetowanie licznika 
  currentEncoderCounts = encoderCounts;
  encoderCounts = 0;
  //Przeliczenie zadanej wartości RPM na odpowiadającą ilość impulsów enkodera 
  setPoint_EncoderCounts = (int)(setPoint_RPM*(FULL_REVOLUTION_COUNTS/(60.0*(1/periodPID)))); 

  //Obliczenie błędu pomiędzy zadaną i aktualną prędkością (jednostka - impulsy enkodera) 
  int error = setPoint_EncoderCounts - currentEncoderCounts;

  //Obliczenie przyrostu całki w ostatnim okresie 
  float curentIntegralPart = error * periodPID;
  integral += curentIntegralPart;

  //Obliczenie członu różnicowego
  float derivative = (error - lastError)/periodPID;
  //Obliczenie wyjścia
  float output = P*error + I*integral + D*derivative;
  
  //Sprawdzenie czy obliczona wartość wyjścia mieści się w przedziale dopuszczalnych wartości
  //Jeśli wyjście jest poza zakresem to zmniejszamy je do granic zakresu 
  //oraz nie pozwalamy rosnąć całce Anti-windup
   
  if(output > MAX_OUTPUT) {
    integral -= curentIntegralPart;
    output = MAX_OUTPUT;
  }
  if(output < -MAX_OUTPUT){
    integral -= curentIntegralPart;
    output = -MAX_OUTPUT;
   
  }
  //Wystawiamy obliczoną wartość PWM z uwzględnieniem kierunku obrotu
  if(output < 0){
    digitalWrite(MOTOR_DIR_PIN, HIGH);
    ledcWrite(MOTOR_PWM_CHANNEL, (int)(-1*output));
  }
  if(output >= 0){
    digitalWrite(MOTOR_DIR_PIN, LOW);
    ledcWrite(MOTOR_PWM_CHANNEL, (int)(output));
  }
  //Zapamiętanie wartości 
//  lastoutput = output;
  lastError = error;

  //Przywrucenie stanu koprocesora po przerwaniu
  if(cp_state) {
    // Restore FPU registers
    xthal_restore_cp0(cp0_regs);
  } else {
    // turn it back off
    xthal_set_cpenable(0);
  }
  
  portEXIT_CRITICAL_ISR(&Mutex);
}
//----------------------------

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

  //Ustawienie pinów połączonych z enkoderem jako wejścia
  pinMode(ROTARYENCODER_A_PIN, INPUT);
  
  //Podłączenie przerwania do pinu  ROTARYENCODER_A_PIN 
  //attachInterrupt("Pin na którego zmiane reagować ma przerwanie, "funkcja która ma zostać wywołana przy uruchomieniu przerwania", "zbocze/stan który ma wywołać przerwanie"
  //W tym przypadku przy każdym wywołaniu przerwania zostanie uruchomiona funkcja encoder_ISR, 
  //natomiast przerwanie wywoła każda zmiana stanu na pinie ROTARYENCODER_A_PIN (zbocze narastające jak i opadające)
  attachInterrupt(digitalPinToInterrupt(ROTARYENCODER_A_PIN), encoder_ISR, CHANGE);
  
  pinMode(ROTARYENCODER_B_PIN, INPUT);
  attachInterrupt(digitalPinToInterrupt(ROTARYENCODER_B_PIN), encoder_ISR, CHANGE);
  
  // Ustawienie Pinu DIR jako wyjście
  pinMode(MOTOR_DIR_PIN, OUTPUT);
  // Ustawienie na Pinie DIR stanu niskiego
  digitalWrite(MOTOR_DIR_PIN, LOW);

  //Konfiguracja wyjścia PWM
  //Ustawinie kanału PWM (Nr kanału, częstotliwość, 10 bitowa rozdzielczość)
  //10 bitowa rozdzielczość daje nam możliwość ustawienia PWM w zakresie 0 - 1023
  ledcSetup(MOTOR_PWM_CHANNEL, 20000, 10);
  //Podłącznie kanału PWM do PINU "MOTOR_PWM_PIN"
  ledcAttachPin(MOTOR_PWM_PIN, MOTOR_PWM_CHANNEL);
  //Ustaienie na wyjściu wypełnia o wartości 0 
//  dutyCycle = 0;
  ledcWrite(MOTOR_PWM_CHANNEL, 0);

  //----------------------------
  //Chcemy ustawić timer tak aby wywoływał funkcję "control" co 10 ms -> 100 Hz
  //inicjalizujemy timer - timerBegin("numer timera (do wyboru 0-3)", "preskaler", "zliczać w górę czy w dół" true = w górę
  //Częstotliwość podłączona do timera to domyślnie 80MHz
  //Przez parametr "preskaler" możemy ją podzielić. Dzielimy ją przez 80 i otrzymujemy 1MHz
  //Timer teraz będzie zwiększał licznik co 1us
  timer = timerBegin(0, 80, true);
  //Ustawiamy w timerze alarm, który będzie wywoływany gdzy licznik osiągnie 10000
  //będzie to skutkowało wywoływaniem alarmu co 10 ms -> 100Hz (1MHz/10000=100Hz)
  //timerAlarmWrite("wskaźnik do timera", "wartość licznika, która ma wywoływać alarm", "Czy po osiągnięciu zadanej wartości zacząć od początku"
  timerAlarmWrite(timer, 10000, true);
  //Podłącznie do alarmu wywołania funkcji "control"
  timerAttachInterrupt(timer, &control, true);
  //Włącznie timera
  timerAlarmEnable(timer);
  //----------------------------

}


int lastTime = millis();
//---------------------
int lastTime2 = millis();
//--------------------
int period = 50; //w [ms]

void loop() {
  //Sprawdź czy przyszła jakaś wiadomość
  if (Serial.available() > 0) {
    //Ustawianie prędkości odbywa się przy użyciu komendy "p100"
    //Po znaku p występuje porządana wartość wypełnienia PWM
    char command = Serial.read();
    //Sprawdź czy wiadomość rozpoczyna się od znaku "p"
    if(command == 'p'){
      //Odczytaj liczbę odebraną przez UART
      int val = Serial.parseInt();
      //-----------------
      //Sprawdź czy wartość mieści się pomiędzy dopuszczalnym minimum i maksimum
      if((val >= -MAX_VELOCITY) && (val <= MAX_VELOCITY)){
        setPoint_RPM = val;
        Serial.print("Ustawiono: ");
        Serial.println(setPoint_RPM);
        //-----------------
      }
      else{
        //Jeśli odebrana wartość nie mieści się w zakresie to wypisz błąd
        Serial.println("Podano złą wartość");
      }   
    }
  }
  
  int timeNow = millis();
  
  if(timeNow - lastTime >= 50){
    //Ustawienia nowego miejsca w czasie od którego liczymy kolejny okres "period"
    lastTime = timeNow;
    //Obliczenie prędkości obrotowej na podstawie zliczeń enkodera
    float rpm = currentEncoderCounts/FULL_REVOLUTION_COUNTS *(1/(periodPID))*60;      
    //-----------------
    //Wypisanie zadanej wartości RPM i zliczeń enkodera w [RPM]
    Serial.printf("%d,%f\n",setPoint_RPM,rpm);
    //----------------
  }
  //-----------------
  if(timeNow - lastTime2 >= 5000){
    lastTime2 = timeNow; 
    setPoint_RPM += change;
    change = -1*change;
  }
  //-----------------
}
